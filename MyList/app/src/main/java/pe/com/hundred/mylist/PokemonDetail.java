package pe.com.hundred.mylist;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

/**
 * Created by flaviofrancotunqui on 4/12/16.
 */
public class PokemonDetail extends AppCompatActivity {

    String name = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pokemon_detail);

        name = getIntent().getStringExtra("name");

        Toast.makeText(PokemonDetail.this, name, Toast.LENGTH_SHORT).show();
    }
}