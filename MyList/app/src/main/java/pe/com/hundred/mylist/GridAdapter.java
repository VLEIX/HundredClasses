package pe.com.hundred.mylist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by flaviofrancotunqui on 4/13/16.
 */
public class GridAdapter extends BaseAdapter {

    private List<Pokemon> pokemonList = new ArrayList<>();
    private Context context;

    public GridAdapter(List<Pokemon> pokemonList, Context context) {
        this.pokemonList = pokemonList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return pokemonList.size();
    }

    @Override
    public Object getItem(int i) {
        return pokemonList.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        View newView = view;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            newView = inflater.inflate(R.layout.pokemon_grill, viewGroup, false);
        }

        ImageView imgPokemon = (ImageView) newView.findViewById(R.id.imgPokemon);

        // getObject
        Pokemon pokemon = pokemonList.get(position);

        // get id from nameImage
        int id = context.getResources().
                getIdentifier(pokemon.getImage(), "drawable", context.getPackageName());

        // assign image
        imgPokemon.setImageResource(id);

        return newView;
    }
}
