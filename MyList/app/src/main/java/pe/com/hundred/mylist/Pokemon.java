package pe.com.hundred.mylist;

/**
 * Created by flaviofrancotunqui on 4/12/16.
 */
public class Pokemon {

    private String name;
    private String nickname;
    private String image;

    public Pokemon(String name, String nickname, String image) {
        this.name = name;
        this.nickname = nickname;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public String getNickname() {
        return nickname;
    }

    public String getImage() {
        return image;
    }
}