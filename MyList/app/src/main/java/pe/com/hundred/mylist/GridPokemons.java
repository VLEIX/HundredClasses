package pe.com.hundred.mylist;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by flaviofrancotunqui on 4/13/16.
 */
public class GridPokemons extends AppCompatActivity {

    GridView gridPokemons;

    List<Pokemon> pokemonList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.grid_pokemons);

        gridPokemons = (GridView) findViewById(R.id.gridPokemons);

        Pokemon pokemon1 = new Pokemon("Pikachu", "Pika", "pokemon_1");
        Pokemon pokemon2 = new Pokemon("Squartle", "Vamo a calmarno", "pokemon_2");
        Pokemon pokemon3 = new Pokemon("Bulbasour", "Bubaaa", "pokemon_3");
        Pokemon pokemon4 = new Pokemon("Charmander", "Chaaa", "pokemon_4");

        pokemonList.add(pokemon1);
        pokemonList.add(pokemon2);
        pokemonList.add(pokemon3);
        pokemonList.add(pokemon4);

        GridAdapter adapter = new GridAdapter(pokemonList, GridPokemons.this);
        gridPokemons.setAdapter(adapter);
    }
}
