package pe.com.hundred.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText edtUser;
    EditText edtPassword;
    Button btnLogin;

    String userDB = "ffranco";
    String passwordDB = "123";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtUser = (EditText) findViewById(R.id.edtUser);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              if (edtUser.getText().toString().equals(userDB) &&
                      edtPassword.getText().toString().equals(passwordDB)) {

                  Intent intent = new Intent(MainActivity.this, Home.class);
                  intent.putExtra("user", edtUser.getText().toString());
                  startActivity(intent);

              } else {
                  Toast.makeText(MainActivity.this, "User incorrect", Toast.LENGTH_SHORT).show();
              }
            }
        });
    }
}