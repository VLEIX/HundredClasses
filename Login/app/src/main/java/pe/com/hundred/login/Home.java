package pe.com.hundred.login;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by flaviofrancotunqui on 4/11/16.
 */
public class Home extends AppCompatActivity {

    TextView lblUser;

    String us;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        lblUser = (TextView) findViewById(R.id.lblUser);

        us = getIntent().getStringExtra("user");

        lblUser.setText(us);
    }
}