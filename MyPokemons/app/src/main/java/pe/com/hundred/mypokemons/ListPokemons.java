package pe.com.hundred.mypokemons;

import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by flaviofrancotunqui on 4/14/16.
 */
public class ListPokemons extends AppCompatActivity {

    ListView listPokemons;

    List<Pokemon> pokemonList = new ArrayList<>();

    ListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_pokemons);

        listPokemons = (ListView) findViewById(R.id.listPokemons);


        // DEMO HARDCODE
//        Pokemon pokemon1 = new Pokemon("Pikachu", "Pika");
//        Pokemon pokemon2 = new Pokemon("Squartle", "Vamo a calmarno");
//        Pokemon pokemon3 = new Pokemon("Bulbasour", "Bubaaa");
//        Pokemon pokemon4 = new Pokemon("Charmander", "Chaaa");
//
//        pokemonList.add(pokemon1);
//        pokemonList.add(pokemon2);
//        pokemonList.add(pokemon3);
//        pokemonList.add(pokemon4);
//

        // get pokemons
        pokemonList = Pokemon.listAll(Pokemon.class);
//        pokemonList = Pokemon.find(Pokemon.class, "NAME LIKE ?", "a" + "%");

        adapter = new ListAdapter(pokemonList, ListPokemons.this);
        listPokemons.setAdapter(adapter);

        listPokemons.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long l) {

                final Pokemon pokemon = pokemonList.get(position);

                //
                AlertDialog.Builder builder = new AlertDialog.Builder(ListPokemons.this);
                builder.setCancelable(false);
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:

                                // delete from DB
                                pokemon.delete();

                                // delete from ArrayList
                                pokemonList.remove(position);

                                // update ListView
                                adapter.notifyDataSetChanged();
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:

                                break;
                        }
                    }
                };

                builder.setMessage("Are you sure to delete this pokemon?")
                        .setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener);
                builder.show();
                //
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_search, menu);

        MenuItem searchItem = menu.findItem(R.id.mnuSearch);
        SearchManager searchManager = (SearchManager) this.getSystemService(SEARCH_SERVICE);

        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
            searchView.setQueryHint("Search pokemon");
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                @Override
                public boolean onQueryTextSubmit(String s) {
                    pokemonList.clear();
                    pokemonList = Pokemon.find(Pokemon.class, "NAME LIKE ?", "%" + s.toLowerCase() + "%");

                    adapter = new ListAdapter(pokemonList, ListPokemons.this);
                    listPokemons.setAdapter(adapter);

                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    return true;
                }
            });

            return true;
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(this.getComponentName()));
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnuSearch:

                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}