package pe.com.hundred.mypokemons;

import com.orm.SugarRecord;

/**
 * Created by flaviofrancotunqui on 4/13/16.
 */
public class Pokemon extends SugarRecord {
    private String name;
    private String nickname;

    public Pokemon() {
    }

    public Pokemon(String name, String nickname) {
        this.name = name;
        this.nickname = nickname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}