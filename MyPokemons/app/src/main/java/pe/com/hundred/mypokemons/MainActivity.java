package pe.com.hundred.mypokemons;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText edtName;
    EditText edtNickname;
    Button btnCatchPokemon;
    Button btnPokemons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtName = (EditText) findViewById(R.id.edtName);
        edtNickname = (EditText) findViewById(R.id.edtNickname);
        btnCatchPokemon = (Button) findViewById(R.id.btnCatchPokemon);
        btnPokemons = (Button) findViewById(R.id.btnPokemons);

        btnCatchPokemon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Pokemon pokemon = new Pokemon(
                        edtName.getText().toString(),
                        edtNickname.getText().toString());

                pokemon.save();

                edtName.setText("");
                edtNickname.setText("");
            }
        });

        btnPokemons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ListPokemons.class);
                startActivity(intent);
            }
        });
    }
}