package pe.com.hundred.linearrelative;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ImageView imgPokemon1;
    ImageView imgPokemon2;
    ImageView imgPokemon3;
    ImageView imgPokemon4;

    ImageView imgPokemonSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imgPokemon1 = (ImageView) findViewById(R.id.imgPokemon1);
        imgPokemon2 = (ImageView) findViewById(R.id.imgPokemon2);
        imgPokemon3 = (ImageView) findViewById(R.id.imgPokemon3);
        imgPokemon4 = (ImageView) findViewById(R.id.imgPokemon4);
        imgPokemonSelected = (ImageView) findViewById(R.id.imgPokemonSelected);

        imgPokemon1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imgPokemonSelected.setImageDrawable(getResources().getDrawable(R.drawable.pokemon_1));

            }
        });

        imgPokemon1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                Toast.makeText(MainActivity.this, "Long press", Toast.LENGTH_SHORT).show();

                return false;
            }
        });

        imgPokemon2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgPokemonSelected.setImageDrawable(getResources().getDrawable(R.drawable.pokemon_2));
            }
        });

        imgPokemon3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgPokemonSelected.setImageDrawable(getResources().getDrawable(R.drawable.pokemon_3));
            }
        });

        imgPokemon4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgPokemonSelected.setImageDrawable(getResources().getDrawable(R.drawable.pokemon_4));
            }
        });
    }
}